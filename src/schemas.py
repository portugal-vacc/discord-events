import base64
import re
from datetime import datetime
from typing import Optional, List, Any

import httpx
from pydantic import BaseModel, validator

CLEAN_DESCRIPTION_EXPRESSION = re.compile("<.*?>")


class EntityMetadata(BaseModel):
    """https://discord.com/developers/docs/resources/guild-scheduled-event
    #guild-scheduled-event-object-guild-scheduled-event-entity-metadata"""

    location: str = None


class GuildScheduledEvent(BaseModel):
    """https://discord.com/developers/docs/resources/guild-scheduled-event
    #guild-scheduled-event-object"""

    id: int
    guild_id: int
    # channel_id: Any
    # creator_id: Any
    name: str
    description: str
    scheduled_start_time: datetime
    scheduled_end_time: datetime
    # privacy_level: Any
    # status: Any
    # entity_type: Any
    # entity_id: Any
    entity_metadata: Optional[EntityMetadata] = None
    # creator: Any
    # user_count: Any
    image: Any

    @property
    def link(self):
        return self.entity_metadata.location


class SendGuildScheduledEvent(BaseModel):
    entity_metadata: EntityMetadata
    name: str
    privacy_level: int = 2
    scheduled_start_time: datetime
    scheduled_end_time: datetime
    description: Optional[str] = None
    entity_type: int = 3
    image: Optional[str] = None  # pass url of banner here

    @validator("name")
    def name_must_be_in_title_form(cls, v):
        return v.title()

    @validator("description")
    def clean_html_from_description(cls, v):
        return re.sub(CLEAN_DESCRIPTION_EXPRESSION, "", v)

    @validator("image")
    def transform_image_url_into_base64(cls, v):
        return f"data:image/png;base64,{base64.b64encode(httpx.get(v).content).decode('utf-8')}"

    class Config:
        json_encoders = {datetime: lambda v: v.isoformat()}


class EventsAirport(BaseModel):
    icao: str


class EventsRoute(BaseModel):
    departure: str
    arrival: str
    route: Any


class EventSchema(BaseModel):
    # id: Any
    # type: str
    # vso_name: Any = None
    name: str
    link: Optional[str] = None
    # organizers: Any = None
    airports: List[Optional[EventsAirport]] = None
    routes: List[Optional[EventsRoute]] = None
    start_time: datetime
    end_time: datetime
    short_description: str
    # description: Any
    banner: str
