from typing import List, Optional

import httpx

from settings import settings
from src.schemas import (
    GuildScheduledEvent,
    SendGuildScheduledEvent,
    EventSchema,
)

HEADERS = {
    "Authorization": f"Bot {settings.discord_application_token}",
    "Content-Type": "application/json",
}
DISCORD_API_BASE_ENDPOINT = "https://discord.com/api/v10"


def list_scheduled_events_for_guild(guild_id: int) -> List[GuildScheduledEvent]:
    response = httpx.get(
        f"{DISCORD_API_BASE_ENDPOINT}/guilds/{guild_id}/scheduled-events",
        headers=HEADERS,
    )
    response.raise_for_status()
    return [GuildScheduledEvent(**e) for e in response.json()]


def publish_guild_scheduled_event(
        guild_id: int, event: SendGuildScheduledEvent
) -> None:
    response = httpx.post(
        f"{DISCORD_API_BASE_ENDPOINT}/guilds/{guild_id}/scheduled-events",
        content=event.model_dump_json(),
        headers=HEADERS,
    )
    response.raise_for_status()


def update_guild_scheduled_event(
        event_id: int, guild_id: int, event: SendGuildScheduledEvent
) -> None:
    response = httpx.patch(
        f"{DISCORD_API_BASE_ENDPOINT}/guilds/{guild_id}/scheduled-events/{event_id}",
        content=event.model_dump_json(),
        headers=HEADERS,
    )
    response.raise_for_status()


def get_my_vatsim_events() -> List[Optional[EventSchema]]:
    response = httpx.get("https://my.vatsim.net/api/v1/events/all")
    events = response.json().get("data")
    return [EventSchema(**event) for event in events]


def is_portugal_event(event: EventSchema) -> bool:
    portugal_in_routes = list(
        filter(lambda route: route.departure.startswith("LP") or route.arrival.startswith("LP"), event.routes))
    portugal_in_airport = list(filter(lambda airport: airport.icao.startswith("LP"), event.airports))
    return True if portugal_in_routes or portugal_in_airport else False


def portugal_events() -> List[Optional[EventSchema]]:
    return list(filter(lambda event: is_portugal_event(event), get_my_vatsim_events()))
