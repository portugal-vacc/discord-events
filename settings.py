from dotenv import load_dotenv
from pydantic_settings import BaseSettings


load_dotenv()


class Settings(BaseSettings):
    debug: bool = False

    api_host: str = "0.0.0.0"
    api_port: int = 8100

    discord_api_base: str = "https://discord.com/api/v10"
    discord_application_token: str
    discord_guild_id: int = 606450565513478144

    class Config:
        extra = "ignore"
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
