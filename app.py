import asyncio
import time
from threading import Thread
from typing import List, Tuple, Any, Optional

import schedule
from asgiref.wsgi import WsgiToAsgi
from flask import Flask, jsonify, Response
from hypercorn import Config
from hypercorn.asyncio import serve

from settings import settings
from src.schemas import SendGuildScheduledEvent, EntityMetadata, GuildScheduledEvent, EventSchema
from src.services import update_guild_scheduled_event, publish_guild_scheduled_event, list_scheduled_events_for_guild, \
    portugal_events

app = Flask(__name__)


def build_scheduled_event_to_discord(
        vatsim_event: EventSchema,
) -> SendGuildScheduledEvent:
    return SendGuildScheduledEvent(
        entity_metadata=EntityMetadata(location=vatsim_event.link),
        name=vatsim_event.name,
        scheduled_start_time=vatsim_event.start_time,
        scheduled_end_time=vatsim_event.end_time,
        description=vatsim_event.short_description,
        image=vatsim_event.banner,
    )


def check_or_return_existing_event(
        vatsim_event: EventSchema, discord_events: List[GuildScheduledEvent]
) -> Tuple[bool, Optional[List]]:
    existing_events = list(filter(lambda discord_event: discord_event.link == vatsim_event.link, discord_events))
    return bool(existing_events), existing_events


def update_events(guild_id: int) -> List[Tuple[str, Any]]:
    try:
        vatsim_events = portugal_events()
        discord_events = list_scheduled_events_for_guild(guild_id)

        change_log = []

        for vatsim_event in vatsim_events:
            event_already_exists, existing_event = check_or_return_existing_event(
                vatsim_event, discord_events
            )
            scheduled_event = build_scheduled_event_to_discord(vatsim_event)

            if event_already_exists:
                update_guild_scheduled_event(
                    event_id=existing_event[0].id,
                    guild_id=guild_id,
                    event=scheduled_event,
                )
                change_log.append(("Event Updated", vatsim_event.name))
            else:
                publish_guild_scheduled_event(guild_id=guild_id, event=scheduled_event)
                change_log.append(("Event Published", vatsim_event.name))
        print(change_log)
        return change_log
    except Exception as expt:
        print(expt)
        pass


@app.route("/events/guild/<guild_id>/update", methods=["GET", "POST"])
def update_or_publish_discord_guild_events(guild_id: int) -> Response:
    change_log = update_events(guild_id)
    return jsonify(change_log)


def run_scheduler():
    update_events(settings.discord_guild_id)
    schedule.every().hour.do(update_events, settings.discord_guild_id)

    while True:
        schedule.run_pending()
        time.sleep(1)


asgi_app = WsgiToAsgi(app)


def run_api():
    config = Config()
    config.bind = [f"{settings.api_host}:{settings.api_port}"]
    asyncio.run(serve(app, config))


def main():
    sheduler_thread = Thread(target=run_scheduler)
    sheduler_thread.start()
    run_api()


if __name__ == "__main__":
    main()
