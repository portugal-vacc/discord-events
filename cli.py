import argparse

from app import update_events

parser = argparse.ArgumentParser(
    prog='Discord Events',
    description='Quick and helpfull commands')

parser.add_argument('guild_id')

args = parser.parse_args()

update_events(args.guild_id)
